--Requête 1: Sélectionner tous les pokemons pouvant apprendre la technique: Pistolet a Eau
select p.nom
from pokemons p
         inner join elements e on e.id = p.element_id
         inner join tech_move tm on e.id = tm.element
where tm.nom == 'Pistolet a Eau';

--Requête 2: Sélectionner tous les pokemons avec leurs éléments, ordonner par élément par ordre décroissant
select p.nom, e.nom
from pokemons p
         inner join elements e on e.id = p.element_id
order by e.nom desc;

--Requête 3: Sélectionne toutes les techniques qui ne sont pas dans hidden move
select *
from tech_move
where id not in (select tech_move_id from hidden_move);

--Requête 4: Lister les pokemons dominants dans l'arene Plante
select *
from pokemons
where element_id == (select element_dominant from arenes where arene == 'Plante')
   or element_id2 == (select element_dominant from arenes where arene == 'Plante')
order by level_depart desc;

--Requête 5: Sélectionner toutes les arènes de la région Kanto
select a.arene, a.element_dominant
from arenes a
         inner join villes v on v.id = a.villes_id
         inner join regions r on r.id = v.regions_id
where r.region == 'Kanto';