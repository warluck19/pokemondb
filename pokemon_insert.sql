pragma foreign_keys = off;
-- Étape 1 : regroupement des delete pour les insert.
delete
from pokemons
;
delete
from elements
;
delete
from regions
;
delete
from villes
;
delete
from arenes
;
delete
from pokemon_sexy
;
delete
from pokemon_laid
;
delete
from tech_move
;
delete
from hidden_move
;

-- Étape 2 : insérer les insert dans les tables.

insert into pokemons(nom, genre, phase_evolution, level_depart, element_id, element_id2, statut)
VALUES ('Bulbizarre', 'mâle et femelle', 'Herbizarre', 1, 9, 10, 'Blessé'),
       ('Herbizarre', 'mâle et femelle', 'Florizarre', 16, 9, 10, 'En forme'),
       ('Florizarre', 'mâle et femelle', null, 32, 9, 10, 'En forme'),
       ('Salamèche', 'mâle et femelle', 'Reptincel', 1, 5, null, 'En forme'),
       ('Reptincel', 'mâle et femelle', 'Dracaufeu', 16, 5, null, 'Blessé'),
       ('Dracaufeu', 'mâle et femelle', null, 36, 5, 15, 'En forme'),
       ('Carapuce', 'mâle et femelle', 'Carabaffe', 1, 3, null, 'En forme'),
       ('Carabaffe', 'mâle et femelle', 'Tortank', 16, 3, null, 'En forme'),
       ('Tortank', 'mâle et femelle', null, 36, 3, null, 'Blessé'),
       ('Chenipan', 'mâle et femelle', 'Chrysacier', 1, 7, null, 'Blessé'),
       ('Chrysacier', 'mâle et femelle', 'Papilusion', 7, 7, null, 'En forme'),
       ('Papilusion', 'mâle et femelle', null, 10, 7, 15, 'En forme'),
       ('Aspicot', 'mâle et femelle', 'Coconfort', 1, 7, 10, 'En forme'),
       ('Coconfort', 'mâle et femelle', 'Dardargnan', 7, 7, 10, 'Blessé'),
       ('Dardargnan', 'mâle et femelle', null, 10, 7, 10, 'En forme');

insert into elements
    (nom)
values ('Combat'),
       ('Dragon'),
       ('Eau'),
       ('Electrique'),
       ('Feu'),
       ('Glace'),
       ('Insecte'),
       ('Normal'),
       ('Plante'),
       ('Poison'),
       ('Psy'),
       ('Roche'),
       ('Sol'),
       ('Spectre'),
       ('Vol')
;

insert into regions
    (region)
values ('Kanto'),
       ('Johto'),
       ('Hoenn'),
       ('Sinnoh'),
       ('Unova'),
       ('Kalos'),
       ('Alola'),
       ('Galar')
;

insert into villes
    (ville, regions_id)
values ('Pallet Town', 1),
       ('Viridian City', 1),
       ('Pewter City', 1),
       ('Cerulean City', 1),
       ('Saffron City', 1),
       ('Vermilion City', 1),
       ('Celadon City', 1),
       ('Lavender Town', 1),
       ('Fuchsia City', 1),
       ('Cinnabar Island', 1),
       ('Bourg Geon', 2),
       ('Ville Griotte', 2),
       ('Mauville', 2),
       ('Ecorcia', 2),
       ('Doublonville', 2),
       ('Rosalia', 2),
       ('Oliville', 2),
       ('Irisia', 2),
       ('Acajou', 2),
       ('Ebenelle', 2)
;

insert into arenes
    (arene, villes_id, element_dominant)
values ('Roche', 3, 12),
       ('Eau', 4, 3),
       ('Electrique', 6, 4),
       ('Plante', 7, 9),
       ('Poison', 9, 10),
       ('Psychique', 5, 11),
       ('Feu', 10, 5),
       ('Sol', 2, 13),
       ('Vol', 13, 15),
       ('Insecte', 14, 7),
       ('Normal', 15, 8)
;

insert into pokemon_sexy
    (pokemon_id)
values (12),
       (15)
;

insert into pokemon_laid
    (pokemon_id)
values (3),
       (10)
;

insert into hidden_move (nom, element, tech_move_id, localisation, region_id)
values ('Coupe', 8, 90, 'Donnée par le Capitaine de l''Océane à Carmin sur Mer', 1),
       ('Vol', 15, 163, 'Offerte dans la partie nord de la Route 16', 1),
       ('Surf', 3, 17, 'Récompense reçue au Parc Safari pour avoir trouvé la Cabane Secrète', 1),
       ('Force', 8, 103, 'Donnée à Parmanie, après avoir retrouvé la Dent d''Or du Gardien', 1),
       ('Flash', 8, 52, 'Donnée par l''assistant du Prof. Chen sur la Route 2', 1),
       ('Ultimapoing', 8, 106, 'Mont Sélénite - Centre Commercial de Céladopole', 1),
       ('Coupe-Vent', 8, 101, 'Centre Commercial de Céladopole - Repère de la Team Rocket sous le Casino', 1),
       ('Danse-Lames', 8, 49, 'Siège social de la Sylphe SARL', 1),
       ('Cyclone', 8, 48, 'Route 4', 1),
       ('Ultimawashi', 8, 113, 'Centre Commercial de Céladopole - Route Victoire', 1)
;

insert into tech_move
    (nom, element, classe, effets_secondaires)
values ('Riposte', 1, 'Physique',
        'Inflige le double des degats subis par une attaque de type Normal ou Combat durant le tour, echoue sinon.'),
       ('Frappe Atlas', 1, 'Physique', 'Inflige des degats egaux au niveau du lanceur.'),
       ('Double Pied', 1, 'Physique', 'Frappe deux fois.'),
       ('Balayage', 1, 'Physique', 'Peut apeurer la cible - Taux de Coups Critiques eleve.'),
       ('Mawashi Geri', 1, 'Physique', 'Peut apeurer la cible.'),
       ('Pied Saute', 1, 'Physique', 'Enleve 1 PV si l attaque echoue.'),
       ('Sacrifice', 1, 'Physique', 'Blesse le lanceur.'),
       ('Pied Voltige', 1, 'Physique', 'Enleve 1 PV si l attaque echoue.'),
       ('Draco-Rage', 2, 'Speciale', 'Inflige toujours 40 PV de degats.'),
       ('Repli', 3, 'Aucune', 'Augmente la defense du lanceur'),
       ('Ecume', 3, 'Speciale', 'Peut baisser la vitesse de la cible.'),
       ('Claquoir', 3, 'Speciale', 'Attaque continue sur deux à cinq tours.'),
       ('Pistolet a Eau', 3, 'Speciale', 'Aucun.'),
       ('Bulles d eau', 3, 'Speciale', 'Peut baisser la vitesse de la cible.'),
       ('Cascade', 3, 'Speciale', 'Aucun.'),
       ('Pince-Masse', 3, 'Speciale', 'Taux de Coups Critiques eleve.'),
       ('Surf', 3, 'Speciale', 'Aucun.'),
       ('Hydrocanon', 3, 'Speciale', 'Aucun.'),
       ('Cage-Eclair', 4, 'Aucune', 'Paralyse la cible.'),
       ('Eclair', 4, 'Speciale', 'Peut paralyser la cible.'),
       ('Poing-Eclair', 4, 'Speciale', 'Peut paralyser la cible.'),
       ('Tonnerre', 4, 'Speciale', 'Peut paralyser la cible.'),
       ('Fatal-Foudre', 4, 'Speciale', 'Peut paralyser la cible.'),
       ('DanseFlamme', 5, 'Speciale', 'Attaque continue sur deux a cinq tours.'),
       ('Flammeche', 5, 'Speciale', 'Peut bruler la cible.'),
       ('Poing de Feu', 5, 'Speciale', 'Peut bruler la cible.'),
       ('Lance-Flamme', 5, 'Speciale', 'Peut bruler la cible.'),
       ('Deflagration', 5, 'Speciale', 'Peut bruler la cible.'),
       ('Brume', 6, 'Aucune', 'Empeche toute varation de stat provoquee par l adversaire.'),
       ('Buee Noire', 6, 'Aucune',
        'Annule toute variation de stat, la confusionm les effets de Puissance, Vampigraine, Mur Lumiere et Protection.'),
       ('Onde Boreale', 6, 'Speciale', 'Peut baisser l attaque de la cible.'),
       ('Poing-Glace', 6, 'Speciale', 'Peut geler la cible.'),
       ('Laser Glace', 6, 'Speciale', 'Peut geler la cible.'),
       ('Blizzard', 6, 'Speciale', 'Peut geler la cible.'),
       ('Secretion', 7, 'Aucune', 'Diminue la vitesse de la cible.'),
       ('Dard Nuee', 7, 'Aucune', 'Attaque deux a cinq fois la cible'),
       ('Vampirisme', 7, 'Physique',
        'Restaure un nombre de PV au lanceur egal a la moitie des degats infliges a la cible.'),
       ('Double-Dard', 7, 'Physique', 'Attaque deux fois - Peut empoisonner.'),
       ('Adaptation', 8, 'Aucune', 'Le lanceur devient du meme type que l adversaire.'),
       ('Affutage', 8, 'Aucune', 'Augmente l attaque du lanceur.'),
       ('Armure', 8, 'Aucune', 'Augmente la defense du lanceur.'),
       ('Berceuse', 8, 'Aucune', 'Endort la cible.'),
       ('Boule Armure', 8, 'Aucune', 'Augmente la defense du lanceur.'),
       ('Brouillard', 8, 'Aucune', 'Baisse la precision de la cible.'),
       ('Clonage', 8, 'Aucune',
        'Cree un clone qui prend les dommages à la place du lanceur, enleve 25% des PV du lanceur.'),
       ('Copie', 8, 'Aucune', 'Apprend une des attaques de l advresaire au hasard jusqu a la fin du combat.'),
       ('Croissance', 8, 'Aucune', 'Augmente le special du lanceur.'),
       ('Cyclone', 8, 'Aucune', 'Met fin au combat contre un Pokemon sauvage, echoue contre un dresseur.'),
       ('Danse-Lames', 8, 'Aucune', 'Augmente l attaque du lanceur de deux niveaux.'),
       ('E-Croques', 8, 'Aucune', 'Restaure jusqu a la moitié des PV du lanceur.'),
       ('Entrave', 8, 'Aucune', 'Supprime durant un a huit tour une attaque au hasard de la cible.'),
       ('Flash', 8, 'Aucune', 'Baisse la precision de la cible.'),
       ('Grincement', 8, 'Aucune', 'Baisse la défense de l adversaire de deux niveaux.'),
       ('Grobisou', 8, 'Aucune', 'Endort la cible.'),
       ('Gros Yeux', 8, 'Aucune', 'Baisse la defense de la cible.'),
       ('Hurlement', 8, 'Aucune', 'Met fin au combat contre un Pokemon sauvage, echoue contre un dresseur.'),
       ('Intimidation', 8, 'Aucune', 'Paralyse la cible.'),
       ('Jet de Sable', 8, 'Aucune', 'Baisse la precision de la cible.'),
       ('Lilliput', 8, 'Aucune', 'Augmente l esquive du lanceur.'),
       ('Metronome', 8, 'Aucune', 'Execute une attaque prise au hasard dans cette liste.'),
       ('Mimi-Queue', 8, 'Aucune', 'Baisse la defense de la cible.'),
       ('Morphing', 8, 'Aucune', 'Transforme le lanceur en le Pokemon adverse.'),
       ('Puissance', 8, 'Aucune', 'Augmente le taux de Coups Critiques du lanceur.'),
       ('Reflet', 8, 'Aucune', 'Augmente l esquive du lanceur.'),
       ('Rugissement', 8, 'Aucune', 'Baisse l attaque de la cible.'),
       ('Soin', 8, 'Aucune', 'Restaure jusqu a la moitie des PV du lanceur.'),
       ('Trempette', 8, 'Aucune', 'Aucun.'),
       ('Ultrason', 8, 'Aucune', 'Rend la cible confuse.'),
       ('Croc Fatal', 8, 'Physique', 'Enleve 50% des PV restants de la cible.'),
       ('Patience', 8, 'Physique',
        'Immobilise le lanceur deux ou trois tours, puis inflige a adversaire le double des degats subis pendant cette periode'),
       ('Sonicboom', 8, 'Physique', 'Inflige toujours 20 PV de degats.'),
       ('Empale Korne', 8, 'Physique',
        'Met l''adversaire KO, echoue s''il a une vitesse superieure à celle du lanceur.'),
       ('Guillotine', 8, 'Physique', 'Met l''adversaire KO, echoue s il a une vitesse superieure à celle du lanceur.'),
       ('Constriction', 8, 'Physique', 'Peut baisser la vitesse de la cible.'),
       ('Etreinte', 8, 'Physique', 'Attaque continue sur deux à cinq tours.'),
       ('Furie', 8, 'Physique', 'Attaque deux a cinq fois.'),
       ('Ligotage', 8, 'Physique', 'Attaque continue sur deux à cinq tours.'),
       ('Pilonnage', 8, 'Physique', 'Attaque deux a cinq fois.'),
       ('Torgnoles', 8, 'Physique', 'Attaque deux a cinq fois.'),
       ('Combo-Griffes', 8, 'Physique', 'Attaque deux a cinq fois.'),
       ('Poing Comete', 8, 'Physique', 'Attaque deux a cinq fois.'),
       ('Frenesie', 8, 'Physique',
        'Attaque sans fin, provoque l''augmentation de l''attaque du lanceur à chaque fois qu''il subit des degats.'),
       ('Picanon', 8, 'Physique', 'Attaque deux a cinq fois.'),
       ('Charge', 8, 'Physique', 'Aucun.'),
       ('Tornade', 8, 'Physique', 'Aucun.'),
       ('Ecras''Face', 8, 'Physique', 'Aucun.'),
       ('Griffe', 8, 'Physique', 'Aucun.'),
       ('Jackpot', 8, 'Physique', 'Fait gagner un peu d''argent a la fin des combats'),
       ('Vive-Attaque', 8, 'Physique', 'Attaque en premier'),
       ('Coupe', 8, 'Physique', 'Aucun.'),
       ('Poing Karate', 8, 'Physique', 'Taux Coups Critiques eleve.'),
       ('Lutte', 8, 'Physique', 'Blesse le lanceur.'),
       ('Force Poing', 8, 'Physique', 'Aucun.'),
       ('Meteores', 8, 'Physique', 'N''echoue jamais.'),
       ('Morsure', 8, 'Physique', 'Peut apeurer la cible.'),
       ('Ecrasement', 8, 'Physique', 'Peut apeurer la cible.'),
       ('Koud''Korne', 8, 'Physique', 'Aucun.'),
       ('Coud''Boule', 8, 'Physique', 'Peut apeurer la cible.'),
       ('Tranche', 8, 'Physique', 'Taux de Coups Critiques eleve.'),
       ('Uppercut', 8, 'Physique', 'Aucun.'),
       ('Coupe-Vent', 8, 'Physique', 'Attaque en deux tours, n''agit pas au premier'),
       ('Croc de Mort', 8, 'Physique', 'Peut apeurer la cible.'),
       ('Force', 8, 'Physique', 'Aucun.'),
       ('Souplesse', 8, 'Physique', 'Aucun.'),
       ('Tripleattaque', 8, 'Physique', 'Aucun.'),
       ('Ultimapoing', 8, 'Physique', 'Aucun.'),
       ('Plaquage', 8, 'Physique', 'Peut paralyser la cible.'),
       ('Belier', 8, 'Physique', 'Blesse le lanceur'),
       ('Mania', 8, 'Physique', 'Attaque continue sur deux ou trois tours, le lanceur devient confus par la suite.'),
       ('Bomb''Oeuf', 8, 'Physique', 'Aucun.'),
       ('Coud''Krane', 8, 'Physique', 'Attaque en deux tours, n''agit pas au premier.'),
       ('Damocles', 8, 'Physique', 'Blesse le lanceur.'),
       ('Ultimawashi', 8, 'Physique', 'Aucun.'),
       ('Destruction', 8, 'Physique',
        'Lanceur KO apres l''attaque, ne tient compte que de la moitie de la defense adverse, ce qui equivaut à une puissance reelle de 260.'),
       ('Ultralaser', 8, 'Physique', 'Lanceur immobilise au tour suivant.'),
       ('Explosion', 8, 'Physique',
        'Lanceur KO après l''attaque, ne tient compte que de la moitie de la defense adverse, ce qui équivaut à une puissance reelle de 340.'),
       ('Para-Spore', 9, 'Aucune', 'Paralyse la cible.'),
       ('Poudre Dodo', 9, 'Aucune', 'Endort la cible.'),
       ('Spore', 9, 'Aucune', 'Endort la cible.'),
       ('Vampigraine', 9, 'Aucune', 'Draine a chaque tour des PV de la cible vers le lanceur.'),
       ('Vol-Vie', 9, 'Speciale',
        'Restaure un nombre de PV au lanceur egal a la moitie des degats infliges a la cible.'),
       ('Fouet Lianes', 9, 'Speciale', 'Aucun.'),
       ('Mega-Sangsue', 9, 'Speciale',
        'Restaure un nombre de PV au lanceur egal a la moitie des degats infliges a la cible.'),
       ('Tranche Herbe', 9, 'Speciale', 'Taux de Coups Critiques eleve.'),
       ('Danse-Fleur', 9, 'Speciale',
        'Attaque continue sur deux ou trois tour, le lanceur devient confus par la suite.'),
       ('Lance-Soleil', 9, 'Speciale', 'Attaque en deux tours, n agit pas au premier.'),
       ('Acidarmure', 10, 'Aucune', 'Augmente la defense du lanceur de deux niveaux.'),
       ('Gaz Toxik', 10, 'Aucune', 'Empoisonne la cible.'),
       ('Poudre Toxik', 10, 'Aucune', 'Empoisonne la cible.'),
       ('Toxik', 10, 'Aucune', 'Empoisonne gravement la cible.'),
       ('Dard-Venin', 10, 'Physique', 'Peut empoisonner la cible.'),
       ('Purepois', 10, 'Physique', 'Peut empoisonner la cible.'),
       ('Acide', 10, 'Physique', 'Peut baisser la defense de la cible.'),
       ('Detritus', 10, 'Physique', 'Peut empoisonner la cible.'),
       ('Amnesie', 11, 'Aucune', 'Augmente le special du lanceur de deux niveaux.'),
       ('Bouclier', 11, 'Aucune', 'Augmente la defense du lanceur de deux niveaux.'),
       ('Hate', 11, 'Aucune', 'Augmente la vitesse du lanceur de deux niveaux.'),
       ('Hypnose', 11, 'Aucune', 'Endort la cible'),
       ('Mur Lumiere', 11, 'Aucune', 'Augmente la resistance du lanceur aux attaques speciales.'),
       ('Protection', 11, 'Aucune', 'Augmente la resistance du lanceur aux attaques physiques.'),
       ('Repos', 11, 'Aucune',
        'Restaure tous les PV du lanceur ainsi que son statut, le lanceur est immobilise pendant deux tours.'),
       ('Telekinesie', 11, 'Aucune', 'Baisse la precision de la cible.'),
       ('Teleport', 11, 'Aucune', 'Met fin au combat cont un Pokemon sauvage, echoue contre un dresseur.'),
       ('Yoga', 11, 'Aucune', 'Augmente l attaque du lanceur.'),
       ('Vague Psy', 11, 'Speciale', 'Inflige des degats variables.'),
       ('Choc Mental', 11, 'Speciale', 'Peut rendre la cible confuse.'),
       ('Rafale Psy', 11, 'Speciale', 'Peut rendre la cible confuse.'),
       ('Psyko', 11, 'Speciale', 'Peut baisser le special de la cible.'),
       ('Devoreve', 11, 'Speciale',
        'Restaure un nombre de PV au lanceur egal a la moitie des degats infliges a la cible, ne fonctionne que si la cible est endormie.'),
       ('Jet-Pierres', 12, 'Physique', 'Aucun.'),
       ('Eboulement', 12, 'Physique', 'Peut apeurer la cible.'),
       ('Abime', 13, 'Physique', 'Met l adversaire KO, echoue s il a une vitesse superieure a celle du lanceur.'),
       ('Osmerang', 13, 'Physique', 'Attaque deux fois.'),
       ('Masse d Os', 13, 'Physique', 'Peut apeurer la cible.'),
       ('Seisme', 13, 'Physique', 'Aucun.'),
       ('Tunnel', 13, 'Physique',
        'Attaque en deux tours, le lanceur ne peut être atteint que par Meteores et Patience au premier tour.'),
       ('Onde Folie', 14, 'Aucune', 'Rend la cible confuse.'),
       ('Tenebres', 14, 'Physique', 'Inflige des degats agaux au niveau du lanceur.'),
       ('Lechouille', 14, 'Physique', 'Peut paralyser la cible.'),
       ('Mimique', 15, 'Aucune', 'Le lanceur utilise la derniere attaque lancee par l adversaire.'),
       ('Cru-Aile', 15, 'Physique', 'Aucun.'),
       ('Picpic', 15, 'Physique', 'Aucun.'),
       ('Vol', 15, 'Physique',
        'Attaque en deux tours, le lanceur ne peut être atteint que par Meteores et Patience au premier tour.'),
       ('Bec Vrille', 15, 'Physique', 'Aucun.'),
       ('Pique', 15, 'Physique', 'Attaque en deux tours, n agit pas au premier.')
;
