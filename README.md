# Lisez-moi

## Auteurs

💻 **Loïc Ménard** *1928991*

💻 **Pierre-Luc Geoffroy** *0770964*

## Description

### Survol

La base de donnée permet de référencer les pokémons de la première génération. Elle inclut une liste des pokémons, leurs mouvements possible, les régions, les villes et les arènes ainsi que les centre de pokémons (pour les soigner). Nous plaçons aussi les pokémons en deux catégories: sexy et laid (parce que pourquoi pas).

### Utilisation

La base de donnée peut être utilisée pour référencer les pokémons et leurs attributs (dans une application web ou android). Elle peut aussi servir à la conception d'un jeu pokémon.



### Précisions:

À la requête numéro 4, un pokemon est dominant dans une arène si L'un de ses éléments correspond à l'élément dominant de l'arène. Exemple: un pokemon possédant un élément eau dominerait dans une arène aquatique.
