pragma foreign_keys = off;

-- Étape 1 : regroupement des drop table.
drop table if exists pokemons;
drop table if exists elements;
drop table if exists regions;
drop table if exists villes;
drop table if exists arenes;
drop table if exists pokemon_sexy;
drop table if exists pokemon_laid;
drop table if exists tech_move;
drop table if exists hidden_move;

pragma foreign_keys = on;

-- Étape 2 : création des tables.

-- Créer la table pokemons. Cette table contient leur noms, le genre masculion ou feminin, la phase d'évolution ainsi
-- que leurx niveaux de départ, les éléments auxquels ils appartiennent et leur statut ou état.
create table if not exists pokemons
(
    id              integer primary key autoincrement,
    nom             text                             not null,
    genre           text,
    phase_evolution text,
    level_depart    integer,
    element_id      integer references elements (id) not null,
    element_id2     integer references elements (id),
    statut          text

);

-- Créer la table elements.
create table if not exists elements
(
    id  integer primary key autoincrement,
    nom text unique not null

);

-- Créer la table regions.
create table if not exists regions
(
    id     integer primary key autoincrement,
    region text
);

-- Créer la table villes. Dans cette table il y a aussi une référence aux régions qu'elles appartiennent.
create table if not exists villes
(
    id         integer primary key autoincrement,
    ville      text,
    regions_id integer references regions (id)
);

-- Créer la table arenes. Cette table contient la localisation de la ville à laquelle est appartient ainsi que son
-- élément dominant.
create table if not exists arenes
(
    id               integer primary key autoincrement,
    arene            text,
    villes_id        integer unique references villes (id),
    element_dominant integer references elements (id) not null
);

-- Créer la table pokemon_sexy, parce que oui il y a certain pokemon qui sont plus sexy que d'autres.
create table if not exists pokemon_sexy
(
    id_sexy    integer primary key autoincrement,
    pokemon_id integer unique references pokemons (id)
);

-- Créer la table pokemon_laid, parce que oui il y a certain pokemon qui sont plus laid que la normal.
create table if not exists pokemon_laid
(
    id_laid    integer primary key autoincrement,
    pokemon_id integer unique references pokemons (id)
);

-- Créer la table tech_move. Cette table contient une liste de techniques employée par les pokémons.
create table if not exists tech_move
(
    id                 integer primary key autoincrement,
    nom                text unique                      not null,
    element            integer references elements (id) not null,
    classe             text                             not null,
    effets_secondaires text                             not null
);

-- Créer la table hidden_move. Cette table contient une liste de techniques secrètes qui peuvent être enseignées aux
-- pokémons et celles-ci ne peuvent s'apprende naturellement. Bien que ces techniques se cachent dans la régon de
-- Kanto, une brève description de la localisation y est insérée.
create table if not exists hidden_move
(
    id           integer primary key autoincrement,
    nom          text unique                       not null,
    element      integer references elements (id)  not null,
    tech_move_id integer references tech_move (id) not null,
    localisation text                              not null,
    region_id    integer references regions (id)   not null


);
